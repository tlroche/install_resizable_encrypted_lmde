+++++++++++++++++++++++++++++++++++++++++++
``cryptsetup benchmark`` and ``luksFormat``
+++++++++++++++++++++++++++++++++++++++++++

.. contents:: **Table of Contents**

summary
=======

Currently, just a few notes about using ``cryptsetup benchmark`` to choose options for ``luksFormat``.

background
==========

To provide disk encryption, `LUKS`_ must provide `the following functionality <https://wiki.archlinux.org/index.php/Disk_encryption#Basic_principle>`_:

* encrypt data written to persistent storage (e.g., hard drive)
* decrypt data read from persistent storage (for use in computation and volatile storage)
* generate one or more keys used for de- and encryption

Each of those functions proceeds at some speed determined by

* the `target device`_ (on which you run LUKS)
* the cryptographic algorithms (or *cryptalgorithms*) you choose for LUKS to employ

LUKS employs cryptalgorithms based on the options you choose when you setup LUKS. Hence you want to choose options that best tradeoff (for you) performance and security. This tradeoff is somewhat subjective, but is also determined by objective relative performance of various cryptalgorithms.

Determining the objective relative performance of various cryptalgorithms also involves tradeoffs. One could most empirically determine the relative performances of each member of a set of cryptalgorithms/options by looping through the following sequence for each member:

1. Install LUKS+LVM2+LMDE on the `target device`_ using the given option set.
2. Use the target device to perform various tasks of concern to you, sampling disk I/O speeds.

But we are impatient. Instead we seek a LUKS `benchmark`_ that we can run once, before installing, using the benchmark's results to guide our choice of cryptalgorithms, and therefore of the LUKS options (or *cryptoptions*) required to operationalize our choice. (For more details regarding LUKS cryptoptions, see `this section of the ArchWiki <https://wiki.archlinux.org/index.php/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode>`_.) The LUKS developers provide ``cryptsetup benchmark`` for our use, so we will use it.

.. _LUKS: https://en.wikipedia.org/wiki/LUKS
.. _benchmark: https://en.wikipedia.org/wiki/Benchmark_%28computing%29

cryptsetup benchmark
====================

installation
------------

If your target already has a `Debian`_ installed:

1. Open a console and try ``sudo which cryptsetup``. If that works, you're "good to go." If that fails,
2. Install package= ``cryptsetup`` available for most releases (e.g., `jessie <https://packages.debian.org/jessie/cryptsetup>`_).

If your target does not have a Debian installed: you should be able to run ``cryptsetup benchmark`` once booted from your `install medium`_, such as an `LMDE LiveUSB`_

.. _Debian: https://en.wikipedia.org/wiki/Debian
.. _LMDE LiveUSB: create_LMDE_liveUSB.rst
.. _glossary: README.rst#glossary
.. _install medium: `glossary`_
.. _target device: `glossary`_

operation
---------

Just open a console and run ``sudo cryptsetup benchmark``\--it's that simple. The default benchmark will typically run in ~10 sec, almost certainly less than a minute. It will produce tabular text output, e.g.,

::

    # Tests are approximate using memory only (no storage IO).
    PBKDF2-sha1       557753 iterations per second
    PBKDF2-sha256     356173 iterations per second
    PBKDF2-sha512     256000 iterations per second
    PBKDF2-ripemd160  336082 iterations per second
    PBKDF2-whirlpool  112219 iterations per second
    #  Algorithm | Key |  Encryption |  Decryption
         aes-cbc   128b   109.7 MiB/s   127.5 MiB/s
     serpent-cbc   128b    42.1 MiB/s   159.5 MiB/s
     twofish-cbc   128b   107.9 MiB/s   141.6 MiB/s
         aes-cbc   256b    86.5 MiB/s    96.0 MiB/s
     serpent-cbc   256b    42.1 MiB/s   160.0 MiB/s
     twofish-cbc   256b   108.2 MiB/s   142.1 MiB/s
         aes-xts   256b   128.4 MiB/s   126.0 MiB/s
     serpent-xts   256b   144.0 MiB/s   148.4 MiB/s
     twofish-xts   256b   132.1 MiB/s   133.5 MiB/s
         aes-xts   512b    96.7 MiB/s    95.2 MiB/s
     serpent-xts   512b   144.7 MiB/s   149.0 MiB/s
     twofish-xts   512b   132.0 MiB/s   133.4 MiB/s

interpretation
--------------

reporting
_________

The default ``cryptsetup benchmark`` output is useful, but could be more so. One can, e.g., manually\ *[1]* format and sort the results to

* better separate de- and encryption speeds
* show all activities in, e.g., order of speed decreasing

producing output like

::

    # key derivation:
    PBKDF2-sha1       557753 iterations per second
    PBKDF2-sha256     356173 iterations per second
    PBKDF2-ripemd160  336082 iterations per second
    PBKDF2-sha512     256000 iterations per second
    PBKDF2-whirlpool  112219 iterations per second

    # encryption:
    #  Algorithm | Key |  Encryption
     serpent-xts   512b   144.7 MiB/s
     serpent-xts   256b   144.0 MiB/s
     twofish-xts   256b   132.1 MiB/s
     twofish-xts   512b   132.0 MiB/s
         aes-xts   256b   128.4 MiB/s
         aes-cbc   128b   109.7 MiB/s
     twofish-cbc   256b   108.2 MiB/s
     twofish-cbc   128b   107.9 MiB/s
         aes-xts   512b    96.7 MiB/s
         aes-cbc   256b    86.5 MiB/s
     serpent-cbc   256b    42.1 MiB/s
     serpent-cbc   128b    42.1 MiB/s

    # decryption:
    #  Algorithm | Key  | Decryption
     serpent-cbc   256b   160.0 MiB/s
     serpent-cbc   128b   159.5 MiB/s
     serpent-xts   512b   149.0 MiB/s
     serpent-xts   256b   148.4 MiB/s
     twofish-cbc   256b   142.1 MiB/s
     twofish-cbc   128b   141.6 MiB/s
     twofish-xts   256b   133.5 MiB/s
     twofish-xts   512b   133.4 MiB/s
         aes-cbc   128b   127.5 MiB/s
         aes-xts   256b   126.0 MiB/s
         aes-cbc   256b    96.0 MiB/s
         aes-xts   512b    95.2 MiB/s

**[1]:** It's not hard using a capable editor, if you know how to use it. That being said, if you've got *code* to automate a transform like the above (it doesn't look hard, I'm just lazy), please either post an `Issue`_, or (better yet) edit this file (adding a link) and make a pull request.

choose cryptoptions
===================

Deriving actual cryptoptions from ``cryptsetup benchmark`` output seems both simple and difficult. A simple approach to the benchmark output

1. takes the benchmarked default options as given. From my limited understanding of cryptology generally and `LUKS`_ specifically, these do seem to be a good set of `ciphers`_, `key`_ sizes, and `key derivation functions`_ *(KDFs)* to benchmark. That being said, ``cryptsetup benchmark`` by default does not benchmark all available combinations of all available cryptoptions\ *[1]*, just "a few common configurations"\ *[2]*.
#. seeks relatively **low** `KDF`_ speed.
#. seeks relatively **high** de/encryption speeds.
#. gives equal weight to de- and encryption speed.
#. chooses to use only `SHA hash functions`_ (out of sheer irrational prejudice?), but ...
#. ... ignores ``sha1``, since it is being phased out

**[1]:** And what are all available cryptoptions? I don't know, but have asked in subquestion#=4.1 of `this SE question`_.

**[2]:** run ``info cryptsetup`` or ``man cryptsetup``

key derivation function
-----------------------

As noted by `another section of the ArchWiki <https://wiki.archlinux.org/index.php/Disk_encryption#Cryptographic_metadata>`_, in "the normal use-case of an authorized user [on a workstation, the [`KDF`_ must only] be calculated once per session." Furthermore, as `noted by Patrick Glandien`_, "[using] a fast hashing algorithm [makes it] easier to crack the passphrase[,] since an attacker can test [more] combinations in [less] time." Glandien further states that

::

    The `--iter-time` option allows you to set the time [to hash the passphrase once].
    The only downside to a high iteration time is that it takes just that long to actually open
    the encrypted device. [If] you use [`--iter-time`=10000, i.e., 10 sec] on your root device,
    then it will take 10 seconds for your system to get the actual encryption key and thus
    you [must] wait that long after [typing your passphrase] before booting continues.
    [Fortunately, you only incur this wait once per boot (unless you mistype your passphrase),
    and] further [session] performance is not affected.

That and the above ``cryptsetup benchmark`` reporting\ *[1]* suggests choosing KDF-related cryptoptions like

* ``--hash=sha512``
* ``--iter-time=10000``

**[1]:** Please remember that the above ``cryptsetup benchmark`` reporting comes from my odd/old hardware, and probably does not resemble what *you* would get when you ``cryptsetup benchmark``! Which is why you should make the minimal effort (documented above) to generate and interpret ``cryptsetup benchmark`` on your own target hardware.

.. _noted by Patrick Glandien: https://unix.stackexchange.com/a/271588/38638
.. _recommended by Patrick Glandien: `noted by Patrick Glandien`_
.. _Per Patrick Glandien: `noted by Patrick Glandien`_

de/encryption
-------------

Choosing decryption and encryption speeds is somewhat more complicated, but one can utter generalities:

1. Relatively **high** de/encryption speeds are preferred, but ...
#. Relatively **secure** de/encryption algorithms are preferred (which tend to be slower).
#. Workload matters: decryption runs on read, encryption runs on write.
#. Security tends to increase with keysize, but performance tends to decrease with keysize.
#. `Per Patrick Glandien`_, as of Mar 2016, "aes-xts-plain64 is considered the most secure LUKS cipher."

Given those heuristics, and given the above ``cryptsetup benchmark`` reporting\ *[1]*, I conclude that

1. decryption: serpent-cbc/256 is fastest, serpent-xts/512 is 3rd fastest
#. encryption: serpent-xts/512 is fastest, serpent-cbc/* is slowest
#. ∴ I should run serpent-xts/512 on this `target device`_.

**[1]:** see previous section.

write cryptoptions
------------------

Given above conclusions, we must translate ``serpent-xts/512`` into LUKS cryptoptions ... and the problem grows more complex. There are 4 basic cryptoptions to set for ``cryptsetup`` (and therefore for your `properties`_):

* ``--cipher``, ``-c``: the default is ``aes-xts-plain64`` per `this section of the ArchWiki <https://wiki.archlinux.org/index.php/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode>`_, so we can guess that we want to use (and I have successfully used on `another box <install_LMDE_plus_LUKS_LVM2.properties.tlroche.tlrW510>`_) ``serpent-xts-plain64``
* ``--key-size``, ``-s``: default is ``256``, looks like we wanna use ``512``
* ``--hash``, ``-h``: given our preference for `SHA hash functions`_ and our desire to use keysize==512, we should probably use ``sha512``
* ``--iter-time``: use large value (c.p.) as noted above

That being said,

* there *are* other/non-default cryptoptions
* It Would Be Nice to know all the available parameters (rather than guessing)
* one's usecase might cause one to give greater weight to decryption speed over encryption speed, or vice versa

For discussion of a more rigorous approach to this question (how *best* to derive cryptoptions from ``cryptsetup benchmark`` output), see `this SE question`_.

.. Note reST syntax (used below, works in restview) for link name reuse

.. _cipher: https://wiki.archlinux.org/index.php/Disk_encryption#Ciphers_and_modes_of_operation
.. _ciphers: `cipher`_
.. _key: https://wiki.archlinux.org/index.php/Disk_encryption#Basic_principle
.. _KDF: https://en.wikipedia.org/wiki/Key_derivation_function
.. _KDFs: `KDF`_
.. _key derivation functions: `KDF`_
.. _SHA hash functions: https://en.wikipedia.org/wiki/Secure_Hash_Algorithm
.. _install\_LMDE\_plus\_LUKS\_LVM2.properties: ./install_LMDE_plus_LUKS_LVM2.properties
.. _properties: `install_LMDE_plus_LUKS_LVM2.properties`_
.. _this SE question: http://unix.stackexchange.com/questions/254017/howto-interpret-cryptsetup-benchmark-results
.. _my SE question: `this SE question`_

TODOs
=====

1. Move these *TODOs* to the `README`_ or to `this project's Issues`_
#. Merge in any good answers to `my SE question`_.
#. Justify or improve my raw-feel choices in `section=choose cryptoptions <#choose-cryptoptions>`_ (above).
#. Document use of non-default cryptoptions.
#. Link to code for ``cryptsetup benchmark`` reporting (possibly after writing it)

.. _README: README.rst
.. _Issue: ../../issues
.. _this project's Issues: Issue_
