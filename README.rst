+++++++++++++++++++++++++++++++++++++++++++++++++
Howto install LMDE on a resizable encrypted drive
+++++++++++++++++++++++++++++++++++++++++++++++++

.. contents:: **Table of Contents**

summary
=======

Current `LMDE installers`_ currently (as of early 2016)

* do not support headless operation
* install only to "normal" drive partitions

This project attempts to make it easier for users to install `LMDE`_

1. headlessly
#. onto resizable "`volumes <https://en.wikipedia.org/wiki/Logical_volume>`_", currently using `LVM2`_
#. onto encrypted partitions, currently using `LUKS`_

until such time as the `LMDE team <http://segfault.linuxmint.com/>`_ can support this functionality, and perhaps to enable them to do so. (My guess is that, the more we as a community can do to solve this problem, the more likely it becomes that the LMDE team  will takeover maintenance.)

Branch= ``master`` should currently allow the user to choose either, both, or none (i.e., to just run the character-mode LMDE installer) of LVM2 and LUKS, but that has not been tested. The following process has been most recently tested with `a MATE LMDE2/Betsy <http://www.linuxmint.com/edition.php?id=188>`_ installing both LVM2 and LUKS. Other news regarding this project may be available on the comment thread for this `LMDE2 tutorial`_.

.. _LMDE: https://en.wikipedia.org/wiki/Linux_Mint#Linux_Mint_Debian_Edition
.. _LMDE installers: http://www.linuxmint.com/download_lmde.php
.. _LMDE2 tutorial: http://forums.linuxmint.com/viewtopic.php?f=241&t=194031
.. _LVM2: https://en.wikipedia.org/wiki/LVM2
.. _LUKS: https://en.wikipedia.org/wiki/LUKS

support
=======

If you have a bug to report or feature to request that is *specific to this code/project*, please first check previously-created `project issues <../../issues>`_ to see if someone else has already reported/requested. Then,

* if there is a relevant current issue: please contribute a comment to it, or just follow the issue.
* if there is *not* a relevant current issue: please `create a new issue <../../issues/new>`_.

For more general comments, complaints, or questions, consider posting to this `LMDE2 tutorial`_. Note that the LMDE2 tutorial also contains information about and links to other manual and automated options for providing this functionality.

implementation
==============

Currently this install

* depends on

    * the Live `LMDE`_ installer (e.g., `LiveCD <https://en.wikipedia.org/wiki/LiveCD>`_, `LiveUSB <https://en.wikipedia.org/wiki/LiveUSB>`_), which in turn depends on the `Debian Live installer <http://live.debian.net/manual/current/html/live-manual/customizing-installer.en.html>`_

* uses

    * a script= `install_LMDE_plus_LUKS_LVM2.sh`_
    * a properties file, e.g. `install_LMDE_plus_LUKS_LVM2.properties`_. Separating the properties (or configuration variables) into a separate file allows users to configure the script for their devices without touching the script itself. Hopefully this will improve maintainability.

This project currently includes 3 examples of properties files:

* `install_LMDE_plus_LUKS_LVM2.properties`_ has (mostly) the original defaults set by `PePas`_
* `install_LMDE_plus_LUKS_LVM2.properties.tlroche.tlrW510`_ has values used by the author when installing LMDE1 (successfully) on an older ThinkPad in Mar 2014 (but format updated to match newer/post-LMDE2 script).
* `install_LMDE_plus_LUKS_LVM2.properties.tlroche.tlrPanP5`_ has values used by the author when installing LMDE2 (successfully) on an even-older System76 Pangolin Performance in Jan 2016.

Feel free to fork this project to send us a pull request with *your* properties file! Note however that `install_LMDE_plus_LUKS_LVM2.sh`_ will want to source ``install_LMDE_plus_LUKS_LVM2.properties`` at runtime, so be sure to make *your* properties file have that name at runtime. (Or edit the script, but I deprecate that.)

.. Note reST syntax (used below, works in restview) for link name reuse

.. _PePas: mailto:solusos@passchier.net
.. _install\_LMDE\_plus\_LUKS\_LVM2.sh: ../HEAD/install_LMDE_plus_LUKS_LVM2.sh
.. _script: `install_LMDE_plus_LUKS_LVM2.sh`_
.. _install script: `install_LMDE_plus_LUKS_LVM2.sh`_
.. _install\_LMDE\_plus\_LUKS\_LVM2.properties: ../HEAD/install_LMDE_plus_LUKS_LVM2.properties
.. _properties: `install_LMDE_plus_LUKS_LVM2.properties`_
.. _install\_LMDE\_plus\_LUKS\_LVM2.properties.tlroche.tlrW510: ../HEAD/install_LMDE_plus_LUKS_LVM2.properties.tlroche.tlrW510
.. _install\_LMDE\_plus\_LUKS\_LVM2.properties.tlroche.tlrPanP5: ../HEAD/install_LMDE_plus_LUKS_LVM2.properties.tlroche.tlrPanP5

instructions
============

glossary
--------

* ``setup device``: the computer on which you create your ``install media`` (below) and edit your install script and properties.
* ``target device``: the computer on which you install LUKS+LVM2+LMDE using your install media, script, and properties (below).
* ``install medium``: drive or disc containing your Live `LMDE`_ installer (e.g., `LiveCD`_, `LiveUSB`_). The following instructions assume you will use an LMDE-enabled LiveUSB (as created by, e.g., `these instructions`_), and that

    * your install media mounts on your setup device at ``/media/you/LMDEliveUSB/``
    * your install media mounts on your target device **at boottime** at a different path than it mounts on your setup device. What exactly that boottime mount path will be depends on details of your installer that are above the level of this discussion. Here, I will assume that the path is like ``/media/mint/LMDEliveUSB/`` (though it may be very different, e.g., ``/lib/live/mount/medium/``).
    * **at boottime**, your target device mounts your install media read-only. (This is standard.)

* ``install script``: `install_LMDE_plus_LUKS_LVM2.sh`_ or similar
* ``install properties``: something like `install_LMDE_plus_LUKS_LVM2.properties`_, but tuned to your usecase
* ``install dir``: a directory/folder (aka "dir") on your install medium for your `script`_ and `properties`_. For this discussion, I'll call that dir

    * as mounted on your setup device: ``/media/you/LMDEliveUSB/scripts/``
    * as boottime-mounted on your target device: ``/media/mint/LMDEliveUSB/scripts/``
    * ignoring context: just ``.../scripts/``

* ``enhanced LMDE LiveUSB``: what you produce from input== `LMDE LiveUSB`_ using the instructions in the `following section <#enhance_installer>`_.

.. _create_LMDE_liveUSB.rst: create_LMDE_liveUSB.rst
.. _LMDE LiveUSB: `create_LMDE_liveUSB.rst`_
.. _these instructions: `create_LMDE_liveUSB.rst`_

enhance installer
-----------------

1. Use your ``setup device`` to create your `LMDE LiveUSB`_.
#. On your ``setup device``, create your install dir.
#. Copy the `script`_ to your install dir. You should not need to edit the script.
#. Copy the `properties`_ to a new file on your install dir, and edit that.

    * I will assume (for documentation below) that you name your properties file ``install_LMDE_plus_LUKS_LVM2.properties.yourbox``
    * In order to choose appropriate de/encryption properties, you should first `benchmark your target device <choose_crypto_options.rst>`_.

I will refer to what you have just created as an ``enhanced LMDE LiveUSB``.

prepare target device
---------------------

check target RAM
~~~~~~~~~~~~~~~~

Though the subject of swap space is controversial, there is wide agreement that the size of your swap partition should `(c.p.) <https://en.wikipedia.org/wiki/Ceteris_paribus>`_ be related to your RAM size. The `install script`_ defaults to a swap partition size that is ``sizeof(RAM) + 100 MB``: if that works for you, then you can skip the rest of this section. Otherwise:

To determine your ``target device``\'s physical RAM size, boot it, and run the following from a terminal:

::

    sudo dmidecode -t memory | fgrep -ie 'size:'

(Note: you will probably need to boot LMDE or another full distro to get ``dmidecode``. The smaller set of utilities provided on the ISOs of tools like (empirically) GParted may not include ``dmidecode``.) This will give you results like the following (from a box with 4 RAM banks, only 2 installed):

::

    Maximum Memory Module Size: 16384 MB
    Maximum Total Memory Size: 65536 MB
    Installed Size: 2048 MB (Single-bank Connection)
    Enabled Size: 2048 MB (Single-bank Connection)
    Installed Size: Not Installed
    Enabled Size: Not Installed
    Installed Size: 2048 MB (Single-bank Connection)
    Enabled Size: 2048 MB (Single-bank Connection)
    Installed Size: Not Installed
    Enabled Size: Not Installed
    Size: 2048 MB
    Size: No Module Installed
    Size: 2048 MB
    Size: No Module Installed

Add the reported sizes (or ``Enabled Size``\s, but some BIOSes empirically don't report that) to get (in this case) 4096 MB. To check your computation, run

::

    fgrep -e 'MemTotal:' /proc/meminfo

in the same shell: the value it reports should **not** be more than the value you compute from ``dmidecode``. FWIW, with the same box, I got

::

    MemTotal:        3988616 kB

so the ``dmidecode``\-reported RAM size==4096 MB seems reasonable. I can therefore use that value to compute a swap partition size, and use that swap size to set the `properties`_ var== ``swap_size``.

partition target drive
~~~~~~~~~~~~~~~~~~~~~~

You will probably also want to repartition (or partition, if new) the ``target device`` drive onto which you intend to install LUKS+LVM2+LMDE. For this example I will assume

#. the drive's device name== ``/dev/sda`` , specified by var== ``grub_device`` in your `properties`_ . You might wish instead to specify the drive by UUID, or by label, or specify another drive by device name, etc.
#. you want to re/partition the drive to have 2 partitions:

    * 1 small unencrypted, unmanaged boot partition. I will further assume that you want size(boot partition)==500 MB.
    * 1 large partition that *will* be encrypted (by `LUKS`_) and managed (by `LVM2`_) to consume the rest of the drive space.

Given those assumptions, you must

* create a 500 MB boot partition in ``/dev/sda1``
* give all remaining space to a single partition== ``/dev/sda2``

unless you have done that already. If not, you can re/partition with various tools, including `GParted`_ and `fdisk`_. Note that all the partition tools of which I'm aware require root, so run them using ``sudo``.

In your properties, you must specify your ``boot_device`` and ``managed_device``, so be sure to record the names created when you re/partition. Note that you should be able to specify your ``boot_device`` and ``managed_device`` by UUID, label, or device name, but only specification by device name has been tested.

GParted
```````

`GParted`_ is an actively developed, user-friendly GUI that is capable of doing this simple task, as well as much more complex partitionings. I recommend its use, especially to beginners, as it can prevent (and often rollback) unfortunate accidents. Fortunately it is also included on bootable installers for most Debian (and some other) distros, including LMDE; you can also make it separately bootable on your `LMDE LiveUSB`_. Note that

1. If you start GParted from your LMDE image (i.e., you use ``GRUB`` to boot to LMDE from your LiveUSB), you should do so by

   1. Open a Terminal (aka console)
   #. Run ``sudo gparted &``

   This will allow you to better track and report any errors that might occur. (You can also start GParted from your window manager's start menu.)
#. GParted may show errors when it attempts to access your LiveUSB. E.g., if you are booting a single-hard-drive laptop from your LiveUSB, the hard drive will be ``/dev/sda`` and your USB drive will be ``/dev/sdb``, and you may get several repeated error dialogs like

    ::

        Error fsyncing/closing /dev/sdb1: Remote I/O error

   Just hit button= ``Ignore``: you don't want to partition your LiveUSB!
#. details of GParted are beyond the scope of this article (feel free to edit and make a pull request!), but you can get much more information about using GParted `here <http://gparted.sourceforge.net/documentation.php>`_.

.. _GParted: http://gparted.sourceforge.net/download.php

fdisk
`````

``fdisk`` is an aging but capable partition tool that runs from the commandline. It is *not* user-friendly and therefore not recommended for beginners. That being said, ``fdisk``

* remains included on bootable installers for virtually every Linux distro
* is easy to mis/use

so an ``fdisk`` run for our example is given here.

1. Boot your ``target device`` with your ``enhanced LMDE LiveUSB``.
#. Open a console and do

    ::

        sudo fdisk /dev/sda [hit Enter]
        o [Enter]
        n [Enter]
          [Enter]
          [Enter]
          [Enter]
        +500M [Enter]
        n [Enter]
          [Enter]
          [Enter]
          [Enter]
          [Enter]
        w [Enter]

run installer
-------------

1. Boot your appropriately-partitioned ``target device`` from your ``enhanced LMDE LiveUSB``.
#. Ensure that the booted OS on your ``target device`` is configured to

   1. provide networking. Your installer will eventually need to download packages.
   #. not disruptively manage power. Use provided power-management configuration tools to ensure that it does not, e.g., sleep on idle.
   #. not disruptively lock. Use provided tools (typically for screensaver configuration) to ensure that it does not, e.g., lock input on idle.

#. Run the script with your properties: using example paths given above,

    ::

        pushd /media/mint/LMDEliveUSB/scripts/
        sudo ./install_LMDE_plus_LUKS_LVM2.sh ./install_LMDE_plus_LUKS_LVM2.properties.yourbox

#. Interact with the script: e.g.,

    * give password for encryption when prompted (twice the same)
    * give password for decryption when prompted (once)

#. Wait a bit for this script to setup your partitions and to start the the LMDE installer, then interact with the non-graphical version of LMDE installer. You will provide the same information as with the GUI installer to which you might be more accustomed, but inside the terminal. (A blast from the past !-)
#. When the script completes, you are back at the command line. Shut down your device (e.g., with ``sudo shutdown -Ph now``) and remove the install media (easier to do with LiveUSBs than LiveCDs at this point), then restart your device.
#. First thing to do on restart: check your mounts! Open a terminal, and run (as a normal user)

    ::

        df -h
        mount
        cat /etc/fstab

.. _LiveCD: https://en.wikipedia.org/wiki/LiveCD
.. _LiveUSB: https://en.wikipedia.org/wiki/LiveUSB

notes on testing
================

power management
----------------

If installing on a laptop, you should probably turn power management off ASAP, so you can close its lid without disrupting the install (by, e.g., {sleep, suspend}ing). Whatever your ``target device``, you should probably disable screensavers and input locks to prevent

* disrupting the install
* inability to update running screensavers or input-management code

undo install
------------

If testing the default configuration (2 partitions, 1st=="normal" ``/boot``, 2nd==managed) you can easily undo a failed install by deleting and recreating the partitions with `GParted`_ or `fdisk`_. E.g., in GParted:

1. Select the 2nd partition. By doing them in reverse order, you will be able to auto-recover your initial size settings (without re-entering them).

   1. Click button= ``Delete`` from the toolbar.
   #. Click button= ``New`` from the toolbar.
   #. Enter partition label from your `properties`_ (e.g., ``LUKS_plus_LVM2``), take other defaults.
   #. Click button= ``Apply`` from the toolbar.

1. Repeat for the 1st partition (but changing the partition label to, e.g., ``boot``).

Note however that you will not want to boot GParted from your ``enhanced LMDE LiveUSB`` just to do this. Instead,

1. boot to LMDE
#. `configure power management <#rst-header-power-management>`_
#. open a terminal, and run ``sudo gparted &``
#. perform the above workflow
#. `run the installer <#rst-header-run-installer>`_

TODO
====

1. Move these *TODOs* to `this project's Issues`_.
#. Need real testcases! All my testing to date has been on 2 boxes with very similar properties. Need to, e.g.,

    * test with defective properties
    * test restart on broken installs

#. both `script`_ and `properties`_: use "real bash booleans" (i.e., ``/bin/false`` and ``/bin/true``)
#. both `script`_ and `properties`_: support option to take ``username`` and ``hostname`` from commandline (so as to not hafta expose them in the properties file).
#. `install_LMDE_plus_LUKS_LVM2.sh`_: test {retval, errorcode}s from all "real" calls (i.e., not messaging or trivial assignments), except

    * *(possibly)* ``cryptsetup luksFormat``: hangs if ``tee``\d, may be resistant to ``eval``
    * ``cryptsetup luksClose`` near end of ``Install_filesystems``: produces lots of fails that seem to have no effect on target device, e.g.

        ::

           device-mapper: remove ioctl on LUKS_plus_LVM2 failed: Device or resource busy

#. `install_LMDE_plus_LUKS_LVM2.sh`_: ensure long-running operations have progress controls (e.g., copying read-only filesystem)
#. `create_LMDE_liveUSB.rst`_: instruct user how to create a non-journaling filesystem that is also not ``ext2``. Consider adapting `these suggestions <http://unix.stackexchange.com/a/259258/38638>`_: basically

    * ``mke2fs -t ext4 -O ^has_journal /dev/whatever``

   Probably don't also wanna do ``tune2fs -E mount_opts=ro /dev/whatever`` since the user would hafta remember to turn that off when adding new ISOs.

.. _Issue: ../../issues
.. _this project's Issues: Issue_
